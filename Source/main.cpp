//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>

// Functions
void countFunction(int intNum);
int factorial(int intNum);

int main (int argc, const char* argv[])
{
    
    int intNum;

    std::cout << "Enter a number\n";                                    //display text
    
    std::cin >> intNum;                                                 //input number
    
//    std::cout << "\nCounting to " << intNum << std::endl;               //feedback
    
//    countFunction(intNum);
    
    std::cout << "\nThe factorial of " << intNum << " is: " << factorial(intNum) <<std::endl;

    
    return 0;
}

void countFunction(int intNum){
    for (int count = 0; count <= intNum; count++) {                     //counting loop
        std::cout << "count " << count << std::endl;                    //display current count on new line
    }
}

int factorial(int intNum){
    if(intNum >= 0){
        int factorialNum;
        for (factorialNum = 1; intNum >= 1; intNum--) {
            factorialNum *=  intNum;
        }
    return factorialNum;
    }
    else{
        std::cout << "use a positive number";
        return 0;
    }
}